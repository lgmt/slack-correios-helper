const axios = require('axios');

async function trackCorreiosPackage(trackingCode) {
    const baseUrl = 'https://api.postmon.com.br/v1/rastreio/ect/';

    try {
        const response = await axios.get(baseUrl + trackingCode);
        const data = response.data;
        if (!data) return 'not-found';

        const postDate = data.historico[0].data;
        const lastUpdate = data.historico[data.historico.length - 1];

        return [postDate, lastUpdate]; //retornar um objeto e nao um array
    } catch (err) {
        return {
            error: true,
            err
        }
    }
}

module.exports = {
    trackCorreiosPackage
}