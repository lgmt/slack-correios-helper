const functions = require('firebase-functions');
const postmon = require('./postmon');

async function correiosCommand(req, res) {
    const text = req.body.text;
    if (text === '') res.send({text: 'Por favor informe um código de rastreio'});

    const tracking = await postmon.trackCorreiosPackage(text.trim().toUpperCase());
    if (tracking === 'not-found') res.send({text: 'Não encontrei nenhuma informação de rastreio para esse código :/'});
    if (tracking.error) res.send({text: `Ocorreu um erro :/\n${tracking.err}`});

    res.send({
        mrkdwn: true,
        text: `Tracking do código ${text.trim().toUpperCase()}
        *Postado dia*: ${tracking[0]}
        *Última atualização*: ${tracking[1].situacao} em ${tracking[1].local}, dia ${tracking[1].data}`
    });
}

module.exports = {
    slackCorreiosCommand: functions.https.onRequest(correiosCommand)
}


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
